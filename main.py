import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
warnings.filterwarnings('ignore')

# Pre-Modeling Tasks
from sklearn.model_selection import train_test_split

# Modeling
from sklearn import linear_model
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier

# Evaluation and comparision of all the models
from sklearn.metrics import precision_score,classification_report
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

# import dataset
features = pd.read_csv("data.csv", header=0, index_col=(0))
# labels
labels = pd.read_csv("labels.csv", header=0, index_col=(0))

# Separating the independant and the dependant variable in our data
X = features
Y = labels.iloc[:, 0]

# Exploratory Data Analysis
print("------------- labels info ------------------")
print("labels.csv : ",Y.info())
print("------------- features info ------------------")
print("features,csv : ",Y.info())
# describing the dataset
print(Y.describe().T)

# dataset dimensions 
print("------------- Dataset dimensions ------------------")
print(X.shape) # number of patients / number of genes
print(Y.shape) # number of patients diagnosed with 5 types of cancer

# Splitting the dataset into training and testing data
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33, random_state=42, stratify=Y)

# Modeling differents models of Machine learning: Logistic Regression, Random Forest, SVM, Decision Tree Classifier
training_accuracy = []
testing_accuracy = []

# Logistic Regression model 
index = [1e-10, 1, 1e10]
# 
for i in index:
   model = linear_model.LogisticRegression(C=i,max_iter=100000)
   # fititng our model 
   model.fit(X_train,y_train)
   training_accuracy.append(accuracy_score(y_train, model.predict(X_train)))
   testing_accuracy.append(accuracy_score(y_test, model.predict(X_test)))
   # Evaluation of the model
   print("------------- Logistic Regression model ----------------------------")
   print("C values : ","1e-10", "1", "1e10")
   print("training accuracy : ",training_accuracy) 
   print("testing accuracy : ",testing_accuracy)
   print("-----------------------------------------")
   print("Crosstab",pd.crosstab(y_test, model.predict(X_test)))
   print("-----------------------------------------")
   print("classification report",classification_report(y_test,model.predict(X_test)))   
   #print(precision_score(y_test,model.predict(X_test),average=None)) 

# visualization of how the training and testing accuracy varies due to different C values
plt.plot(index, training_accuracy, label = 'Training')
plt.plot(index, testing_accuracy, label = 'Testing') 
# x axis scale
plt.xlim([0, 3])
# y axis scale
plt.ylim([0, 1.5])
# naming the x axis
plt.xlabel('C values')
# naming the y axis
plt.ylabel('accuracy')
# giving a title to my graph
plt.title('Evolution of accuracy score with the increase of the C parameter for the Logistic Regression model')
plt.legend()
plt.show()

# =============================================================================
# SVM
svc_model = SVC()
svc_model.fit(X_train, y_train)
y_predict = svc_model.predict(X_test)

# Evaluation of the model
print("------------- SVM model ----------------------------")
print("accuracy :",accuracy_score(y_test,y_predict))
print("-----------------------------------------")
print("Crosstab",pd.crosstab(y_test,y_predict))
print("-----------------------------------------")
print("classification report",classification_report(y_test,y_predict))
#print(precision_score(y_test,y_predict,average=None))


# =============================================================================
# Random forest model
RF = RandomForestClassifier(n_estimators = 10, criterion = 'entropy', random_state = 0)
RF.fit(X_train, y_train)
y_predict_rf = RF.predict(X_test)

# Evaluation of the model
print("------------- RANDOM FOREST model ----------------------------")
print("accuracy :",accuracy_score(y_test,y_predict_rf))
print("-----------------------------------------")
print("Crosstab",pd.crosstab(y_test,y_predict_rf))
print("-----------------------------------------")
print("classification report",classification_report(y_test,y_predict_rf))
print("-----------------------------------------")
#print(precision_score(y_test,y_predict_rf,average=None))

# =============================================================================
# Decision Tree Classifier
dt = DecisionTreeClassifier()
dt.fit(X_train, y_train)
y_pred_dt = dt.predict(X_test)

# Evaluation of the model
print("------------- Decison Tree model ----------------------------")
print("accuracy :",accuracy_score(y_test,y_pred_dt))
print("-----------------------------------------")
print("Crosstab",pd.crosstab(y_test,y_pred_dt))
print("-----------------------------------------")
print("classification report",classification_report(y_test,y_pred_dt))
print("-----------------------------------------")
#print(precision_score(y_test,y_predict_dt,average=None))

# ===================== comparaison of all of the models ===============================
#models = []
#Z = [SVC() , DecisionTreeClassifier() , LogisticRegression() ,RandomForestClassifier() ]
#X = ["SVC" , "DecisionTreeClassifier" , "LogisticRegression" , "RandomForestClassifier" ]
#for i in range(0,len(Z)):
    #model = Z[i]
    #model.fit( X_train , y_train )
    #pred = model.predict(X_test)
    #models.append(accuracy_score(pred , y_test))   
#d = { "Accuracy" : models , "Algorithm" : X }
#data_frame = pd.DataFrame(d)
#sns.barplot(data_frame,x='Accuracy',y='Algorithm',palette= "husl").set_title('Accuracy of all Algorithms')
# =============================================================================
