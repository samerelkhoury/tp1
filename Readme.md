# Cancer-Type Predictions using Machine Learning

## Introduction

The main purpose of this project is to predict a cancer type using gene expression levels of a variety of genes of interest in the most 5 diffrent cancer types : BRCA,COAD,KIRC,LUAD and PRAD.

### Script
- main.py

### Programming language 
- python 3.11.0

```{}
sudo apt update
sudo apt -y upgrade
python3 -V
```

## Installation

- You can install the package from the source code hosted on github.

```bash
git clone git@gitlab.com:samerelkhoury/tp1.git
```

## Getting started

## Prerequisites

### Data Installation :

- You can install the used dataset in the link below
[gene expression cancer RNA-Seq Data Set] (https://archive.ics.uci.edu/ml/datasets/gene+expression+cancer+RNA-Seq)

### Conda Environment Installation and Activation :

Using the following commands 

```bash
conda create -n environmentName python=3.11.0
conda activate environmentName
```

### libraries to install :

- scikit-learn	1.2.0
- tensorflow  	1.8.0
- matplotlib  	3.6.2
- pandas      	1.5.2
- seaborn     	0.12.2

Please make sure you're conda environment is activated befor using the following commands 

```bash
pip install scikit-learn=1.2.0
pip install tensorflow=1.8.0
pip install matplotlib=3.6.2
pip install pandas=1.5.
pip install seaborn=0.12.2
```

## Guide 

## Run Program 

- make sure all the downloaded files are in the same directory
- click run on the **main.py** file to launch your program in your IDE or launch the script in your terminal 
Using the following command

```bash
python3 main.py
```

## 4 ML models were compared:
1. Logistic Regression
2. Multiclass logistic regression (MLR)
3. Support vector machines (SVM)
4. Random Forest (RF)

## Tips 

For more information please refer to the e21226203.pdf report

## Authors 

Samer El Khoury 
- email : <samer.EL-KHOURY@etu.univ-amu.fr>

## Now You Are Ready To Go !



















